#ifndef __VEK_UTILS_H__
#define __VEK_UTILS_H__

#include <stdint.h>
#include <stdbool.h>
#include "Arduino.h"

//for visualStduio debug
#if 0
#include <Windows.h>
#include <mmsystem.h>
#pragma comment (lib, "winmm.lib")
#endif

#define VEK_CH1		(1)
#define VEK_CH2		(2)

#define STICK_X		ch1
#define STICK_Y		ch2

#define MOTOR_L		ch1
#define MOTOR_R		ch2

#define SYSTIMER_1MS		(millis())

#define PWM_INPUT_MAX			(1920)
#define PWM_INPUT_MIN			(1120)
#define PWM_INPUT_MID			(1520)

#define UTILS_VAL_MAX			((int16_t)1000)
#define UTILS_VAL_MIN			((int16_t)-1000)
#define UTILS_VAL_MID			((int16_t)0)

#define UTILS_VAL_DEADZONE_H	(UTILS_VAL_MID+100)
#define UTILS_VAL_DEADZONE_L	(UTILS_VAL_MID-100)

#define CHANNEL_N	(2)
typedef int16_t		CHANNEL_VAL_TYPE;

struct STRUCT_ctrl
{
	CHANNEL_VAL_TYPE ch1;
	CHANNEL_VAL_TYPE ch2;
};

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void convPWM2ctrlVal(int16_t in, uint8_t ch, struct STRUCT_ctrl *out);
void applyDeadZone(struct STRUCT_ctrl *in, struct STRUCT_ctrl *out,uint8_t ch);
void applyDeadTimeAtReversal(struct STRUCT_ctrl *in, struct STRUCT_ctrl *out, uint16_t deadTime);
void calcSteering2motor_onestick(struct STRUCT_ctrl *in, struct STRUCT_ctrl *out);
void applyRampUpDown(struct STRUCT_ctrl *in, struct STRUCT_ctrl *out, uint8_t ch, uint8_t timeSpan, uint16_t upRatio, uint16_t downRatio);
void swapChannel(struct STRUCT_ctrl *in, struct STRUCT_ctrl *out);
void signRevers(struct STRUCT_ctrl *in, struct STRUCT_ctrl *out, uint8_t ch);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
