#include "DualVNH5019MotorShieldMod.h"
#include "VEK_utils.h"

#define VEK_RR_M_INA  (29)
#define VEK_RR_M_ENA  (28)
#define VEK_RR_M_INB  (27)
#define VEK_RF_M_INA  (24)
#define VEK_RF_M_ENA  (26)
#define VEK_RF_M_INB  (25)

#define VEK_LR_M_INA  (32)
#define VEK_LR_M_ENA  (33)
#define VEK_LR_M_INB  (34)
#define VEK_LF_M_INA  (35)
#define VEK_LF_M_ENA  (36)
#define VEK_LF_M_INB  (37)

#define VEK_RR_M_CS   (A3)
#define VEK_RF_M_CS   (A2)
#define VEK_LR_M_CS   (A1)
#define VEK_LF_M_CS   (A0)

#define VEK_LF_M_PWM  (44)
#define VEK_RF_M_PWM  (8)
#define VEK_LR_M_PWM  (7)
#define VEK_RR_M_PWM  (6)

#define VEK_LED1    (45)
#define VEK_LED2    (46)
#define VEK_LED3    (47)
#define VEK_LED4    (48)
#define VEK_LED5    (49)

DualVNH5019MotorShieldMod mdR(VEK_RF_M_INA, VEK_RF_M_INB, VEK_RF_M_ENA, VEK_RF_M_CS, VEK_RF_M_PWM,
                                 VEK_RR_M_INA, VEK_RR_M_INB, VEK_RR_M_ENA, VEK_RR_M_CS, VEK_RR_M_PWM);
//mdL.m1=LF mdL,m2=LR
DualVNH5019MotorShieldMod mdL(VEK_LF_M_INA, VEK_LF_M_INB, VEK_LF_M_ENA, VEK_LF_M_CS, VEK_LF_M_PWM,
                                 VEK_LR_M_INA, VEK_LR_M_INB, VEK_LR_M_ENA, VEK_LR_M_CS, VEK_LR_M_PWM);
//mdR.m1=RF mdR,m2=RR

const uint16_t  PWM1ReadPin = 2;  //CPPM_IN
const uint16_t  PWM2ReadPin = 3;  //CPPM_IN
const uint16_t  LED1Pin = 45;
const uint16_t  LED2Pin = 46;
const uint16_t  LED3Pin = 47;

//PWM入力1の上限・下限・中立・デッドゾーンの定義
#define PWM_1_INPUT_MAX      (1920)
#define PWM_1_INPUT_MIN     (1120)
#define PWM_1_INPUT_MID     (1520)
#define PWM_1_OUTPUT_LIMIT    (100)
#define PWM_1_INPUT_DEADZONE_H  (PWM_1_INPUT_MID+20)
#define PWM_1_INPUT_DEADZONE_L  (PWM_1_INPUT_MID-20)

//PWM入力2の上限・下限・中立・デッドゾーンの定義
#define PWM_2_INPUT_MAX     (1920)
#define PWM_2_INPUT_MIN     (1120)
#define PWM_2_INPUT_MID     (1520)
#define PWM_2_INPUT_DEADZONE_H  (PWM_2_INPUT_MID+20)
#define PWM_2_INPUT_DEADZONE_L  (PWM_2_INPUT_MID-20)

//UART入力の上限・下限・中立の定義
#define UART_COMMAND_MAX    (2000)
#define UART_COMMAND_MID    (1500)
#define UART_COMMAND_MIN    (1000)

//超信地旋回しきい値
#define PWM_2_INPUT_LEFT_SPINTURN   (1420)
#define PWM_2_INPUT_RIGHT_SPINTURN  (1620)

//モータドライバへの出力上限値(最大400)
#define PWM_MOTOR_DUTY_MAX (400)

//PWM1のパルス幅測定処理
uint32_t  PWM1_pulseTimeH_start, PWM1_pulseTimeH_end;
uint32_t  PWM1_pulseTimeH;
bool      oldPWMIN1Pin_state, PWMIN1Pin_state;
bool      fCapturePWM1 = 0;
void capturePWM1()
{
  PWMIN1Pin_state = digitalRead(PWM1ReadPin);
  if ( PWMIN1Pin_state == 1 )
  {
    PWM1_pulseTimeH_start = micros();
  }
  else if ( PWMIN1Pin_state == 0)
  {
    PWM1_pulseTimeH_end = micros();
    PWM1_pulseTimeH = PWM1_pulseTimeH_end - PWM1_pulseTimeH_start;
    if(PWM1_pulseTimeH > PWM_1_INPUT_MAX)
    {
      PWM1_pulseTimeH = PWM_1_INPUT_MAX;
    }
    if(PWM1_pulseTimeH < PWM_1_INPUT_MIN)
    {
      PWM1_pulseTimeH = PWM_1_INPUT_MIN;
    }
    fCapturePWM1 = 1;
  }
}

uint32_t  PWM2_pulseTimeH_start, PWM2_pulseTimeH_end;
uint32_t  PWM2_pulseTimeH;
bool      oldPWMIN2Pin_state, PWMIN2Pin_state;
bool      fCapturePWM2 = 0;
void capturePWM2()
{
  PWMIN2Pin_state = digitalRead(PWM2ReadPin);
  if ( PWMIN2Pin_state == 1 )
  {
    PWM2_pulseTimeH_start = micros();
  }
  else if ( PWMIN2Pin_state == 0)
  {
    PWM2_pulseTimeH_end = micros();
    PWM2_pulseTimeH = PWM2_pulseTimeH_end - PWM2_pulseTimeH_start;
    if(PWM2_pulseTimeH > PWM_2_INPUT_MAX)
    {
      PWM2_pulseTimeH = PWM_2_INPUT_MAX;
    }
    if(PWM2_pulseTimeH < PWM_2_INPUT_MIN)
    {
      PWM2_pulseTimeH = PWM_2_INPUT_MIN;
    }
    fCapturePWM2 = 1;
  }
}

void stopIfFault()
{
  if(mdR.getM1Fault() | mdR.getM2Fault())
  {
      if (mdR.getM1Fault())
      {
        Serial2.println("M1 fault");
      }
      if (mdR.getM2Fault())
      {
        Serial2.println("M2 fault");
      }
      mdR.setM1Speed(0);
      mdR.setM2Speed(0);
      digitalWrite(LED1Pin, 1);
  }
  if(mdL.getM1Fault() | mdL.getM2Fault())
  {
      if (mdL.getM1Fault())
      {
        Serial2.println("M1 fault");
      }
      if (mdL.getM2Fault())
      {
        Serial2.println("M2 fault");
      }
      mdL.setM1Speed(0);
      mdL.setM2Speed(0);
      digitalWrite(LED1Pin, 1);
  }
  else
  {
    digitalWrite(LED1Pin, 0);  
  }
}


void setup() {
  Serial.begin(115200);
  Serial2.begin(115200);
  pinMode(PWM1ReadPin, INPUT_PULLUP);
  pinMode(PWM2ReadPin, INPUT_PULLUP);
  mdR.init();
  mdL.init();
  delay(2000);  //受信機起動待ち
  attachInterrupt(digitalPinToInterrupt(PWM1ReadPin), capturePWM1, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PWM2ReadPin), capturePWM2, CHANGE);
  Serial2.println("Start...");
}

uint32_t  PWMWatchTimer = 0;
uint32_t  UARTtimer = 0;
uint32_t  oldPWM1_pulseTimeH_end=0, oldPWM2_pulseTimeH_end=0;

uint8_t cNoCapturePWM1 = 0,cNoCapturePWM2 = 0;
bool fNoCapturePWM1 = 0,fNoCapturePWM2 = 0;

struct STRUCT_ctrl ctrl;
struct STRUCT_ctrl propo_in;
  
void loop() {
  //PWM無入力判定
  if( (PWMWatchTimer + 100) <= millis())
  {
    PWMWatchTimer = millis();
    if(oldPWM1_pulseTimeH_end == PWM1_pulseTimeH_end)
    {
      cNoCapturePWM1++;
    }
    else
    {
      cNoCapturePWM1=0;
    }
    oldPWM1_pulseTimeH_end = PWM1_pulseTimeH_end;
    
    if(oldPWM2_pulseTimeH_end == PWM2_pulseTimeH_end)
    {
      cNoCapturePWM2++;
    }
    else
    {
      cNoCapturePWM2=0;
    }
    oldPWM2_pulseTimeH_end = PWM2_pulseTimeH_end;
    
    if( (cNoCapturePWM1>=5) || (cNoCapturePWM2>=5) )
    {
      if(cNoCapturePWM1>=5) 
      {
        fNoCapturePWM1 = 1;
      }
      else
      {
        //Serial2.println("PWM2 No signal!");
        fNoCapturePWM2 = 1;
      }
    }
    else
    {
      fNoCapturePWM1 = 0;
      fNoCapturePWM2 = 0;
    }
  }

  //受信したパルス幅から戦車モード用モータPWMデューティ計算
  if ((fCapturePWM1 == 1) || (fCapturePWM2 == 1))
  {
    fCapturePWM1 = 0;
    fCapturePWM2 = 0;
    convPWM2ctrlVal(PWM1_pulseTimeH, VEK_CH1, &propo_in);
    convPWM2ctrlVal(PWM2_pulseTimeH, VEK_CH2, &propo_in);
    applyDeadZone(&propo_in, &propo_in, VEK_CH1);
    applyDeadZone(&propo_in, &propo_in, VEK_CH2);
  }

  applyRampUpDown(&propo_in,&ctrl,VEK_CH1,5,10,10);
  applyRampUpDown(&propo_in,&ctrl,VEK_CH2,5,10,10);
  calcSteering2motor_onestick(&ctrl, &ctrl);

#if 1
  if(ctrl.MOTOR_R == 0)
  {
      mdR.setM1Brake(400);
      mdR.setM2Brake(400);
  }
  else
  {
    mdR.setM1Speed(ctrl.MOTOR_R/2.5);
    mdR.setM2Speed(ctrl.MOTOR_R/2.5);
  }

  if(ctrl.MOTOR_L == 0)
  {
      mdL.setM1Brake(400);
      mdL.setM2Brake(400);
  }
  else
  {
    mdL.setM1Speed(ctrl.MOTOR_L/2.5);
    mdL.setM2Speed(ctrl.MOTOR_L/2.5);
  }
  stopIfFault();
#endif

#if 1
  if ( (UARTtimer + 500) <= millis())
  {
    UARTtimer = millis();

    Serial2.print("PWM1:");
    Serial2.print(PWM1_pulseTimeH, DEC);
    Serial2.print(", ");
    Serial2.print("PWM2:");
    Serial2.print(PWM2_pulseTimeH, DEC);
    
    Serial2.print(", propoCH1:");
    Serial2.print(propo_in.ch1, DEC);
    Serial2.print(", propoCH2:");
    Serial2.print(propo_in.ch2, DEC);

    Serial2.print(", motorCH1:");
    Serial2.print(ctrl.ch1, DEC);
    Serial2.print(", motorCH2:");
    Serial2.print(ctrl.ch2, DEC);
    
    Serial2.println();
  }
#endif
}

