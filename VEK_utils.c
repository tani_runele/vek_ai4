#include "VEK_utils.h"

#define _USE_MATH_DEFINES
#include <math.h>
extern Serial2;

void convPWM2ctrlVal(int16_t in, uint8_t ch, struct STRUCT_ctrl *out)
{
	CHANNEL_VAL_TYPE tmp;
	if (in > PWM_INPUT_MID)
	{
		tmp = ((int32_t)UTILS_VAL_MAX * (int32_t)(in - PWM_INPUT_MID)) / (PWM_INPUT_MAX - PWM_INPUT_MID);
	}
	else if (in < PWM_INPUT_MID)
	{
		tmp = ((int32_t)UTILS_VAL_MIN * (int32_t)(PWM_INPUT_MID - in)) / (PWM_INPUT_MID - PWM_INPUT_MIN);
	}
	else
	{
		tmp = 0;
	}
 
	if (tmp > UTILS_VAL_MAX)
	{
		tmp = UTILS_VAL_MAX;
	}
	else if (tmp < UTILS_VAL_MIN)
	{
		tmp = UTILS_VAL_MIN;
	}

	switch (ch)
	{
		case 1:
			out->ch1 = tmp;
			break;
		case 2:
			out->ch2 = tmp;
			break;
	}
}

void applyDeadZone(struct STRUCT_ctrl *in, struct STRUCT_ctrl *out,uint8_t ch)
{
	if(ch == 1)
	{
		if (in->ch1 > UTILS_VAL_DEADZONE_H)
		{
			out->ch1 = ((int32_t)(UTILS_VAL_MAX - UTILS_VAL_MID)*(int32_t)(in->ch1 - UTILS_VAL_DEADZONE_H)) / (UTILS_VAL_MAX - UTILS_VAL_DEADZONE_H);
		}
		else if (in->ch1 < UTILS_VAL_DEADZONE_L)
		{
			out->ch1 = ((int32_t)(UTILS_VAL_MIN - UTILS_VAL_MID)*(int32_t)(in->ch1 - UTILS_VAL_DEADZONE_L)) / (UTILS_VAL_MIN - UTILS_VAL_DEADZONE_L);
		}
		else
		{
			out->ch1 = UTILS_VAL_MID;
		}
	}
	else if(ch == 2)
	{
		if (in->ch2 > UTILS_VAL_DEADZONE_H)
		{
			out->ch2 = ((int32_t)(UTILS_VAL_MAX - UTILS_VAL_MID)*(int32_t)(in->ch2 - UTILS_VAL_DEADZONE_H)) / (UTILS_VAL_MAX - UTILS_VAL_DEADZONE_H);
		}
		else if (in->ch2 < UTILS_VAL_DEADZONE_L)
		{
			out->ch2 = ((int32_t)(UTILS_VAL_MIN - UTILS_VAL_MID)*(int32_t)(in->ch2 - UTILS_VAL_DEADZONE_L)) / (UTILS_VAL_MIN - UTILS_VAL_DEADZONE_L);
		}
		else
		{
			out->ch2 = UTILS_VAL_MID;
		}
	}
}

void applyDeadTimeAtReversal(struct STRUCT_ctrl *in, struct STRUCT_ctrl *out, uint16_t deadTime)
{
	static uint32_t timerStart;
	uint8_t fDeadTimeEnable = 0;
	static uint8_t fDeadTimeActive = 0;
	static CHANNEL_VAL_TYPE old_MotorLeftDuty=0, old_MotorRightDuty=0;
	
	if ((in->MOTOR_L != 0) || (in->MOTOR_R != 0))
	{
		fDeadTimeEnable = 1;
	}
	if (fDeadTimeEnable == 1)
	{
		if ((((old_MotorLeftDuty > 0) && (in->MOTOR_L < 0))
			|| ((old_MotorLeftDuty < 0) && (in->MOTOR_L > 0))
			|| ((old_MotorRightDuty > 0) && (in->MOTOR_R < 0))
			|| ((old_MotorRightDuty < 0) && (in->MOTOR_R > 0))
			|| ((in->MOTOR_L == 0) && (in->MOTOR_R == 0)))
			&& (fDeadTimeActive == 0))
		{
			fDeadTimeActive = 1;
			timerStart = SYSTIMER_1MS;
		}

		old_MotorLeftDuty = in->MOTOR_L;
		old_MotorRightDuty = in->MOTOR_R;

		if (fDeadTimeActive == 1)
		{
			out->MOTOR_L = 0;
			out->MOTOR_R = 0;
			if ((timerStart + deadTime) <= SYSTIMER_1MS)
			{
				fDeadTimeActive = 0;
				fDeadTimeEnable = 0;
			}
		}
		else
		{
			out->MOTOR_L = in->MOTOR_L;
			out->MOTOR_R = in->MOTOR_R;
		}
	}
}

void calcSteering2motor_onestick(struct STRUCT_ctrl *in, struct STRUCT_ctrl *out)
{
	float	infr, inlr, angmul, revangmul;
	float	throttle, angle, outl, outr;

	infr = ((float)in->STICK_Y)/ UTILS_VAL_MAX;
	inlr = ((float)in->STICK_X)/ UTILS_VAL_MAX;

	throttle = sqrt(infr*infr + inlr*inlr);
	if (throttle >= 1.0) {
		throttle = 1.0;
	}
	angle = atan2(inlr, infr) / (M_PI / 4);
	//angle:
	//  12 oclock 0
	//   3 oclock +2000
	//   6 oclock +/-4000
	//   9 oclock -2000

	angmul = fmod(fabs(angle), 1);
	revangmul = 1 - angmul;
	outl = outr = throttle;

	if (
		(angle >= 0 && angle<1.000)    //00:00-01:29
	) {
		outr *= revangmul;
	}else 
	if ((angle >= 1.000 && angle<2.000) //01:30-02:59
	) {
		outr *= -angmul;
	}else
	if (
		(angle >= 2.000 && angle<3.000) //03:00-04:29
	) {
		outl *= revangmul;
		outr *= -1;
	}else
	if (
		(angle >= 3.000 && angle<4.000) //04:30-05:59
	) {
		outl *= -angmul;
		outr *= -1;
	}else
	if (
		(angle >= 4.000)                //06:00
	){
		outl *= -1;
		outr *= -1;
	}else
	if (
		(angle <= 0 && angle>-1.000)    //10:30-12:00
		) {
		outl *= revangmul;
	}else
	if (
		(angle <= -1.000 && angle>-2.000) //09:00-10:30
		) {
		outl *= -angmul;
	}else
	if (
		(angle <= -2.000 && angle>-3.000) //07:30-09:00
		) {
		outr *= revangmul;
		outl *= -1;
	}else
	if (
		(angle <= -3.000 && angle>-4.000) //06:00-07:30
		) {
		outr *= -angmul;
		outl *= -1;
	}
	out->MOTOR_L = (int)(outl * UTILS_VAL_MAX);
	out->MOTOR_R = (int)(outr * UTILS_VAL_MAX);
}

void applyRampUpDown(struct STRUCT_ctrl *in, struct STRUCT_ctrl *out, uint8_t ch, uint8_t timeSpan, uint16_t upRatio, uint16_t downRatio)
{
	static CHANNEL_VAL_TYPE outputVal[CHANNEL_N] = { 0 };
	static uint32_t timerStart[CHANNEL_N]={0};
	int16_t select_chVal;

	ch = ch -1;
	if (ch == 0)
	{
		select_chVal = in->ch1;
	}
	else if (ch == 1)
	{
		select_chVal = in->ch2;
	}

	if(outputVal[ch] != select_chVal )
	{
		if((timeSpan + timerStart[ch]) <= SYSTIMER_1MS)
		{
			timerStart[ch] = SYSTIMER_1MS;
			if (outputVal[ch] < select_chVal)
			{
				if(upRatio != 0)
				{
					outputVal[ch] = outputVal[ch] + upRatio;
				}
				else
				{
					outputVal[ch] = select_chVal;
				}
				if (outputVal[ch] > UTILS_VAL_MAX)
				{
					outputVal[ch] = UTILS_VAL_MAX;
				}
			}
			else
			{
				if(downRatio != 0)
				{
					outputVal[ch] = outputVal[ch] - downRatio;
				}
				else
				{
					outputVal[ch] = select_chVal;
				}
				if (outputVal[ch] < UTILS_VAL_MIN)
				{
					outputVal[ch] = UTILS_VAL_MIN;
				}
			}
		}
	}

	if (ch == 0)
	{
		out->ch1 = outputVal[ch];
	}
	else if (ch == 1)
	{
		out->ch2 = outputVal[ch];
	}
}

void swapChannel(struct STRUCT_ctrl *in, struct STRUCT_ctrl *out)
{
	CHANNEL_VAL_TYPE tmp;
	tmp = in->ch1;
	out->ch1 = in->ch2;
	out->ch2 = tmp;
}

void signRevers(struct STRUCT_ctrl *in, struct STRUCT_ctrl *out, uint8_t ch)
{
	if (ch == 1)
	{
		out->ch1 = -1 * in->ch1;
	}
	else if (ch == 2)
	{
		out->ch2 = -1 * in->ch2;
	}
}
